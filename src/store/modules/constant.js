const Constant = {
  opearte_constant: {
    UNKNOWN_OPERATE: 0,
    ADD_OPERATE: 1,
    DEL_OPERATE: 2,
    UPD_OPERATE: 3,
    SEND_REPORT_OPERATE: 4,
    CANCEL_OPERATE: 5,
    SHOW_REPORT_OPERATE: 6,
    DOWN_REPORT_OPERATE: 7
  },
  hit_constant: {
    HIT: 1, // 是槽口
    QUERY: 2, // 是查询词语
    JUMP : 3 // 是跳转
  }
}

export default Constant