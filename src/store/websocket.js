var WebSocketManager = {
  instance: null,
  socketStatus: null,
  successCallback: null,
  initWebSocket: function (wsUrl, uid) {
    let $this = this
    if ($this.socketStatus) {
      console.log('websocket已建立')
      return
    }
    const wsuri = wsUrl + uid // wss地址
    console.log("wsuri:"+wsuri)
    // 判断当前浏览器是否支持WebSocket
    if ('WebSocket' in window) {
      let websocket = new WebSocket(wsuri)
      // 连接成功建立的回调方法
      websocket.onopen = function (event) {
        $this.socketStatus = true
        console.log('websocket已启用')
      }
      websocket.onerror = function (e) {
        $this.socketStatus = false
        console.log('与后台握手出现问题')
      }
      // 接收到消息的回调方法
      websocket.onmessage = function (event) {
        $this.socketStatus = true
        console.log('onmessage:' + JSON.stringify(event.data))
        if ($this.successCallback instanceof Function) {
          $this.successCallback(event.data)
        }
      }
      // 连接关闭的回调方法
      websocket.onclose = function () {
        $this.socketStatus = false
        console.log('正在关闭 webSocket ')
      }
      setInterval(function () {
        if (websocket != null && $this.socketStatus) {
          websocket.send('web请求保持socket建立:' + uid)
        }
      }, 5000)
      $this.instance = websocket
    } else {
      alert("若您在使用'秘塔小程序'语音搜索，当前浏览器不支持，请切换浏览器")
    }
  },
  sendMsg: function (msg) {
    this.instance.send(msg)
  }
}
export default WebSocketManager
