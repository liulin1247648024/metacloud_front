// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'
import Vue from 'vue'
import App from './App'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import store from './store'
import Filters from '@/utils/filter'
import VueCookies from 'vue-cookies'
import UUID from 'vue-uuid'
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'
import WebSocketManager from '@/store/websocket'

Vue.prototype.WebSocketManager = WebSocketManager
Vue.use(ElementUI)
Vue.use(Filters)
Vue.use(VueCookies)
Vue.use(UUID)

Vue.config.productionTip = false

// 定义全局变量，为highlight数据进行处理，使数据最后自动添加...省略号
Vue.prototype.addelide = function (value) {
  if (value) {
    return value + '... '
  }
}

// 初始化sentry
if (config.deploytype !== 'develop') {
  Sentry.init({
    dsn: config.dsn,
    integrations: [
      new Integrations.Vue({
        Vue,
        attachProps: true
      })
    ]
  })
}

/* eslint-disable no-new */
const globalRouter = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

export default globalRouter
