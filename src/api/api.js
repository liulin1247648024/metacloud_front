import axios from 'axios'
import * as types from '@/store/mutation-types'
import globalRouter from '@/main'

axios.defaults.baseURL = config.axiosUrl
axios.defaults.timeout = 50000
axios.defaults.withCredentials = true

const defaultHeaders = {
  Accept: 'application/json, text/plain, */*; charset=utf-8',
  'Content-Type': 'application/json; charset=utf-8',
  Pragma: 'no-cache',
  'Cache-Control': 'no-cache'
}
// 设置默认头
Object.assign(axios.defaults.headers.common, defaultHeaders)

axios.interceptors.response.use(
  response => {
    judgeResult(response.data)
    return response
  },
  error => {
    return Promise.reject(error.response) // 返回接口返回的错误信息
})

export function judgeResult (result) {
  if (result.errCode === -100) {
    return false
  }
  return true
}

const typeIsNumber = (val) => {
  return typeof val === 'number'
}
