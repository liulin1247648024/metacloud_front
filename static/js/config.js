/**
 * 配置一些全局的公共变量,便于生产环境和线上环境切换
 */

// eslint-disable-next-line no-unused-vars
var config = {}

if (location.host === 'metasotalaw.cn') {
  // 测试环境配置
  config = {
    domain: 'metasotalaw.cn',
    appid: 'wx7bae32381026968e',
    redirect_uri: 'https%3a%2f%2fmetasotalaw.cn%2flogin',
    mbappid: 'wx189505c2a87c8360',
    mbredirect_uri: 'https%3a%2f%2fmetasotalaw.cn%2flogin',
    mbcallback_uri: 'https%3A%2F%2Fmetasotalaw.cn%2F%40%2Fmblogin%3Forigin%3Dmb',
    translateWindowUrl: 'https://fanyi.metasotalaw.cn/#/simpletranslation',
    translateUrl: 'https://fanyi.metasotalaw.cn/',
    ocr: 'https://ocr.metasotalaw.cn/#/',
    axiosUrl: 'https://metasotalaw.cn/',
    metacloud_url: 'https://cloud.metasotalaw.cn/metacloud/',
    metacloud_file_url: 'http://39.104.59.2:8002/file/preview/',
    unauthorized: true,
    dsn: 'https://0840c43ddd104d96a383465ed18d7ec9@sentry.metasotalaw.com/10',
    ws_url: 'wss://metasotalaw.cn/recorder/websocket/',
    showKnowledgePackage: true
  }
} else if (location.host === 'metaso.cn') {
  // 线上环境配置
  config = {
    domain: 'metaso.cn',
    appid: 'wx58123abae5d5658b',
    redirect_uri: 'https%3a%2f%2fmetaso.cn%2flogin',
    mbappid: 'wx189505c2a87c8360',
    mbredirect_uri: 'https%3a%2f%2fmetaso.cn%2flogin',
    mbcallback_uri: 'https%3A%2F%2Fmetaso.cn%2F%40%2Fmblogin%3Forigin%3Dmb',
    translateWindowUrl: 'https://fanyi.metaso.cn/#/simpletranslation',
    translateUrl: 'https://fanyi.metaso.cn/',
    ocr: 'https://ocr.metaso.cn/#/',
    axiosUrl: 'https://metaso.cn/',
    metacloud_url: 'https://cloud.metaso.cn/metacloud/',
    metacloud_file_url: '',
    unauthorized: true,
    dsn: 'https://d211a17cf14f4cd8a15fffeaae5dc4ab@sentry.metasotalaw.com/11',
    ws_url: 'wss://metaso.cn/recorder/websocket/',
    showKnowledgePackage: true
  }
} else if (location.host === 'metaso.local') {
  // 内部环境配置
  config = {
    domain: 'metaso.local',
    appid: 'wx58123abae5d5658b',
    redirect_uri: 'http%3a%2f%2fmetaso.local%2flogin',
    mbappid: 'wx189505c2a87c8360',
    mbredirect_uri: 'http%3a%2f%2fmetaso.local%2flogin',
    mbcallback_uri: 'http%3A%2F%2Fmetaso.local%2F%40%2Fmblogin%3Forigin%3Dmb',
    translateWindowUrl: 'http://fanyi.metaso.local/#/simpletranslation',
    translateUrl: 'http://fanyi.metaso.local/',
    ocr: 'http://ocr.metaso.local/#/',
    axiosUrl: 'http://metaso.local/',
    metacloud_url: 'http://cloud.metaso.local/metacloud/',
    metacloud_file_url: '',
    unauthorized: true,
    ws_url: 'wss://metaso.local/recorder/websocket/',
    showKnowledgePackage: false
  }
} else {
  // 本地开发环境
  config = {
    domain: location.host + ':' + location.port,
    appid: 'wx7bae32381026968e',
    redirect_uri: 'https%3a%2f%2ftest.metasotalaw.cn%2flogin',
    mbappid: 'wx7bae32381026968e',
    mbredirect_uri: 'https%3a%2f%2ftest.metasotalaw.cn%2flogin',
    mbcallback_uri: 'https%3A%2F%2Ftest.metasotalaw.cn%2F%40%2Fmblogin%3Forigin%3Dmb',
    translateWindowUrl: 'https://fanyi.test.metasotalaw.cn/#/simpletranslation',
    translateUrl: 'https://fanyi.test.metasotalaw.cn/',
    metacloud_url: 'https://cloud.metasotalaw.cn/metacloud/',
    metacloud_file_url: 'http://39.104.59.2:8002/file/preview/',
    axiosUrl: '/api',
    unauthorized: false,
    showKnowledgePackage: true
  }
}
